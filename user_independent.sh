#!/bin/bash
bash gitconfig.sh
bash solarized.sh

cp ./.bash_aliases ~/.bash_aliases

cd ~
git clone https://gitlab.com/catstyle.lee/vim.git vim
cd vim
git submodule update --init --recursive
ln -s vim/.vimrc .
ln -s vim/.vim .
cd ..

# install pyenv
git clone https://github.com/yyuu/pyenv.git ~/.pyenv
echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.bashrc
echo 'export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.bashrc
echo 'eval "$(pyenv init -)"' >> ~/.bashrc
git clone https://github.com/yyuu/pyenv-virtualenv.git ~/.pyenv/plugins/pyenv-virtualenv
echo 'eval "$(pyenv virtualenv-init -)"' >> ~/.bashrc
exec "$SHELL"


# powerline
mkdir -p ~/.config
cp -r .config/powerline ~/.config
