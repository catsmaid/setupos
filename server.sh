#!/bin/bash
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
apt-key fingerprint 0EBFCD88
add-apt-repository \
    "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
    $(lsb_release -cs) \
    stable"
apt-get update
apt-get install -y docker-ce

# install google protobuf
mkdir -p /opt/repositories
chmod a+wx /opt/repositories
cd /opt/repositories
git clone https://github.com/google/protobuf.git
cd protobuf
cpu=`cat /proc/cpuinfo | grep processor | wc -l`
./autogen.sh && ./configure && make -j $cpu && make install && ldconfig
