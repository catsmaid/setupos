apt-get update
apt-get install -y aptitude

echo 'export LC_ALL=en_US.UTF-8' >> /etc/bash.bashrc
locale-gen en_US.UTF-8 zh_CN.UTF-8
dpkg-reconfigure locales

aptitude install -y python3-software-properties software-properties-common
aptitude install -y build-essential automake autoconf libtool
aptitude install -y vim git universal-ctags curl unzip

aptitude upgrade -y
