#!/bin/bash
git config --global core.ignorecase false
git config --global core.filemode false
git config --global core.autocrlf input
git config --global credential.helper store
