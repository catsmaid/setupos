#!/bin/bash
aptitude install -y python3 python3-dev python3-pip python3-virtualenv flake8

# install latest packages via pip
pip3 install setuptools ipython  -U
pip3 install powerline-status powerline-gitstatus
