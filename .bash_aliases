alias l="ls -CF --color=auto"
alias ls="ls -h --color=auto"
alias ll="ls -alFh --color=auto"
alias grep="grep --color=auto -n"

alias findf="find . -type f -print0 | xargs -0 grep --color=auto -n "
alias findjs="find . -name '*.js' -print0 | xargs -0 grep --color=auto -n "
alias findpy="find . -name '*.py' -print0 | xargs -0 grep --color=auto -n "
alias findrb="find . -name '*.rb' -print0 | xargs -0 grep --color=auto -n "
alias findphp="find . -name '*.php' -print0 | xargs -0 grep --color=auto -n "

alias rmpyc="find . -name '*.pyc' | xargs rm "
alias rmpyo="find . -name '*.pyo' | xargs rm "

export PROTOCOL_BUFFERS_PYTHON_IMPLEMENTATION=cpp
export PYTHONPATH=.
export EDITOR=/usr/bin/vim

# used with findxxx above
# usage: findpy xxx | xvim
function xvim() {
    awk -F: '{print $1}' | uniq | xargs -o vim
}

# usage: findpy xxx | xsed -i s/abc/cba/g
function xsed() {
    awk -F: '{print $1}' | uniq | xargs sed $@
}

function set_http_proxy() {
    export http_proxy=http://$1;
    export https_proxy=http://$1;
    export HTTP_PROXY=http://$1;
    export HTTPS_PROXY=http://$1;
}

function unset_http_proxy() {
    unset http_proxy;
    unset https_proxy;
    unset HTTP_PROXY;
    unset HTTPS_PROXY;
}

function set_git_proxy() {
    git config http.proxy socks5://$1;
    git config https.proxy socks5://$1;
}

function unset_git_proxy() {
    git config --unset http.proxy;
    git config --unset https.proxy;
}
