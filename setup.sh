#!/bin/bash
bash basic.sh
bash server.sh
bash python.sh

# enable powerline
echo "powerline-daemon -q" >> /etc/bash.bashrc
echo "POWERLINE_BASH_CONTINUATION=1" >> /etc/bash.bashrc
echo "POWERLINE_BASH_SELECT=1" >> /etc/bash.bashrc

repository_root=$(pip3 show powerline-status | grep Location | grep -P "/.+" -o| cut -d: -f2)
echo ". $repository_root/powerline/bindings/bash/powerline.sh" >> /etc/bash.bashrc

# cp /etc/sysctl.conf /etc/sysctl.conf.bak
# cp ./sysctl.conf /etc/sysctl.conf
# sysctl -p

# curl -sSL https://get.daocloud.io/daotools/set_mirror.sh | sh -s http://1a9c6f1d.m.daocloud.io
# systemctl restart docker
