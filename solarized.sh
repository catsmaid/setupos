#/bin/bash
cd /tmp
git clone https://github.com/seebi/dircolors-solarized.git

#Solarized Dark:
cp dircolors-solarized/dircolors.ansi-dark ~/.dircolors
#Solarized Light:
#cp dircolors-solarized/dircolors.ansi-light ~/.dircolors

#added to .bashrc/.profile if needed
eval `dircolors ~/.dircolors`

#Then, set up Solarized for GNOME Terminal:
git clone https://github.com/Anthony25/gnome-terminal-colors-solarized.git
cd gnome-terminal-colors-solarized

#And now you can set it to light or dark using the following commands:
./set_dark.sh
#./set_light.sh
